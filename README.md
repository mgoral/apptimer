apptimer
=======

Simple script which creates and manages a database of times for which process
has been run.

It's useful e.g. for measuring how much time you spend playing a game. ;)

## Usage:

    $ apptimer run <command>

    $ apptimer show
    $ apptimer show <command name>

    $ apptimer rm <command name>

    $ apptimer merge <command name 1> <command name 2> ...

## Examples:

    $ apptimer run sleep 12s
    $ apptimer show sleep
    /bin/sleep: 12s

    $ apptimer run sleep 4s
    $ apptimer show sleep
    /bin/sleep: 16s

    $ apptimer run ls /
    bin boot dev etc ...

    $ apptimer show
    /bin/ls: 0s
    /bin/sleep: 16s

    $ apptimer rm /bin
    $ apptimer show
    [nothing is shown, because everything's removed]
